package com.ftw.binar8apr

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_coffee.view.*
import kotlinx.android.synthetic.main.dialog_edit_coffee.view.*


class MainActivity : AppCompatActivity() {
    private val GALLERY_REQUEST_CODE = 100
    private var uriPhoto: Uri? = null
    private val mDialogView by lazy {
        LayoutInflater.from(this).inflate(R.layout.dialog_coffee, null)
    }
    private val mDialogEdit by lazy {
        LayoutInflater.from(this).inflate(R.layout.dialog_edit_coffee, null)
    }
    private lateinit var adapterRecycleView: AdapterRv

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapterRecycleView = AdapterRv()

        rv_main.layoutManager =
            LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
        rv_main.adapter = adapterRecycleView

        adapterRecycleView.setOnClickListener(object : AdapterRv.OnClickListenerCoffee {
            override fun onClickCallback(coffee: Coffee, position: Int) {
                showDialogEdit(coffee, position)
            }
        })

        fab_addCoffee.setOnClickListener {
            showDialog()
        }
    }

    private fun showDialog() {
        //untuk menghilangkan error "The specified child already has a parent. You must call removeView() on the child's parent first"
        if (this.mDialogView.parent != null) {
            val viewGroup = this.mDialogView.parent as ViewGroup
            viewGroup.removeView(mDialogView)
        }

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setTitle("Masukan item")
        val mAlertDialog = mBuilder.show()

        mDialogView.btnAddItem.setOnClickListener {
            val coffee = Coffee(
                mDialogView.etTitle.text.toString(),
                mDialogView.etPrice.text.toString(),
                uriPhoto!!
            )
            adapterRecycleView.addCoffee(coffee)
            mAlertDialog.dismiss()
        }
        mDialogView.img_selected_product_photo.setOnClickListener {
            pickFromGallery()
        }
        mDialogView.btnCancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    private fun showDialogEdit(coffee: Coffee, position: Int) {
        if (mDialogEdit.parent != null) {
            val viewGroup = mDialogEdit.parent as ViewGroup
            viewGroup.removeView(mDialogEdit)
        }
        val mBuilder = AlertDialog.Builder(this@MainActivity)
            .setView(mDialogEdit)
            .setTitle("Edit Item")
        val mAlertDialog = mBuilder.show()

        mDialogEdit.etEditTitle.setText(coffee.title)
        mDialogEdit.etEditPrice.setText(coffee.price)
        mDialogEdit.imgEdit_selected_product_photo.setImageURI(coffee.image)

        mDialogEdit.btnEditAddItem.setOnClickListener {
            val coffee = Coffee(
                mDialogEdit.etEditTitle.text.toString(),
                mDialogEdit.etEditPrice.text.toString(),
                uriPhoto!!
            )
            adapterRecycleView.updateCoffee(coffee, position)
            mAlertDialog.dismiss()
        }
        mDialogEdit.imgEdit_selected_product_photo.setOnClickListener {
            pickFromGallery()
        }
        mDialogEdit.btnRemove.setOnClickListener {
            adapterRecycleView.removeCoffee(position)
            mAlertDialog.dismiss()
        }
        mDialogEdit.btnEditCancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                //data.getData returns the content URI for the selected Image
                val selectedImage: Uri? = data?.data
                Glide.with(this).load(selectedImage).into(mDialogView.img_selected_product_photo)
                Glide.with(this).load(selectedImage).into(mDialogEdit.imgEdit_selected_product_photo)
                uriPhoto = selectedImage
            }
        }
    }

    private fun pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        val intent = Intent(Intent.ACTION_PICK)
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.type = "image/*"
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        val mimeTypes =
            arrayOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        // Launching the Intent
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }
}
