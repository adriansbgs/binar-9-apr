package com.ftw.binar8apr

import android.net.Uri

data class Coffee (val title: String, val price: String, val image: Uri)