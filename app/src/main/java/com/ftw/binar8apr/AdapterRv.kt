package com.ftw.binar8apr

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class AdapterRv() : RecyclerView.Adapter<AdapterRv.MainViewHolder>() {

    private lateinit var listener : OnClickListenerCoffee
    fun setOnClickListener (listenerCoffee: OnClickListenerCoffee) {
        this.listener = listenerCoffee
    }
    private val data: MutableList<Coffee> = mutableListOf()

    fun updateCoffee(coffee: Coffee, position: Int) {
        this.data[position] = coffee
        notifyDataSetChanged()
    }

    fun addCoffee(coffee: Coffee) {
        this.data.add(coffee)
        notifyDataSetChanged() //gunanya untuk me-refresh data ketika ditambah
    }

    fun removeCoffee(position: Int) {
        this.data.removeAt(position)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return MainViewHolder(v)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(data[position], position)
    }

    inner class MainViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val image = view.findViewById<ImageView>(R.id.img_product_photo)
        val title = view.findViewById<TextView>(R.id.tv_title)
        val price = view.findViewById<TextView>(R.id.tv_price)

        fun bind(coffee: Coffee, position: Int) {
            title.text = coffee.title
            price.text = coffee.price
            Glide.with(view.context)
                .load(coffee.image)
                .into(image)
            listener.let {
                itemView.setOnClickListener {
                    listener.onClickCallback(coffee,position)
                }
            }
        }
    }

    interface OnClickListenerCoffee {
        fun onClickCallback(coffee: Coffee, position: Int)
    }
}